#!/bin/bash
sleep 15
URL=$URL
COUNTRESPONSE="0"
echo "TEST D'INITIALISATION DU COMPTEUR"
$COUNTRESPONSE=$((curl -sb -H "$URL/count" | jq -r '.pingCount'))

echo "Reponse de la requete count :"
echo $COUNTRESPONSE
COUNTMODEL="0"
echo "Reponse attendue :"
echo $COUNTMODEL
if [ "$COUNTRESPONSE" = "$COUNTMODEL" ]
then 
  echo "Le compteur est bien initialisé a 0"
else 
  echo "Le compteur n'est pas bien initialisé, veuillez reinitialiser le compteur"
fi
echo "TEST DE LA REQUETE PING"
RESPONSE=$((curl -sb -H "$URL/ping" | jq -r '.message'))
echo "Reponse de la requete ping :"
echo $RESPONSE
PINGMODEL="pong"
echo "Reponse attendue :"
echo $PINGMODEL
if [ "$RESPONSE" = "$PINGMODEL" ]
then 
  echo "la fonction ping retourne bien ce qui est attendu"
else 
  echo "la fonction ping ne retourne pas ce qui est attendu"
fi
echo "Lancement de 3 requetes pings supplémentaire pour le test suivant."

curl -sb -H "$URL/ping"
curl -sb -H "$URL/ping"
curl -sb -H "$URL/ping"

echo "Nouveau test de la requete count."
COUNTRESPONSE=$((curl -sb -H "$URL/count" | jq -r '.pingCount'))
echo "Reponse de la requete count :"


echo $COUNTRESPONSE
COUNTMODEL="4"
echo "Reponse attendue :"
echo $COUNTMODEL
if [ "$COUNTRESPONSE" = "$COUNTMODEL" ]
then 
  echo "Le compteur est bien à la valeur attendue"
else 
  echo "Le compteur n'est pas à la valeur attendue"
fi
