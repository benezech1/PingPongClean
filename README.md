# Groupe 3 : Renault - Benezech

## Projet Docker en Symfony

L'application est en Standalone, 

    Require: Php => 7.0.0
    On Kali linux: php-curl    

Pour la lancer il faut aller dans le dossier du projet puis lancer les commandes:

    php composer.phar install
    php bin/console server:start

La gestion du compteur est gerée avec une variable de session, elle est donc unique à chaque utilisateur et chaque fichier bash.

Pour lancer les test jq est nécessaire :

    apt-get install jq
    
Pour lancer le test il faut passer en argument l'url du serveur

    exemple: ./automaticTest.sh http://127.0.0.1:8000
    
Test merge request 

 